# Shared Task Statistics #

In this repo, we annotated several aspects for several shared tasks.

### Contents ###

* CSV: the annotated files (cleaned a bit)
* splittasks.py: used to split the CSV files, which makes it easier to gather statistics
* The other directories contain the data, splitted by venue, year and task.

### Annotation Scheme ###

* First three columns specify if people affiliated to a company were part of the team, can be: 'no company', 'some company' or 'company'. Only one of these three should get the binary value 1.
* The fourth column is also binary, this represents organisers participation.
* The fifth column is paper submitted.
* Finally we included the ranking of the team

### Conferences ###

* CLEF: 2015 - 2016:  http://ceur-ws.org/Vol-1391/
* CONLL: 2012 - 2016: http://www.aclweb.org/anthology/K/K12/#2012_1
* EVALITA: 2016: http://ceur-ws.org/Vol-1749/
* SEMEVAL: 2014 - 2016: http://www.aclweb.org/anthology/S/S14/
* WMT: 2016: http://www.statmt.org/wmt16/papers.html


### Reproduce ###
To get the statistics from the paper, simply run:
```
python3 scripts/generalStats.py clef conll evalita semeval wmt
```

For any questions, contact Rob van der Goot:

* r.van.der.goot@rug.nl