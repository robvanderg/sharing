import csv
import sys
import os


if len(sys.argv) < 2:
    print('specify folder(s) with annotated files, ie:')
    print(sys.argv[0] + ' semeval conll')
    exit(1)
dataPos = [0, 0, 0, 0, 0]
dataPosNorm = [0, 0, 0, 0, 0]
dataParts = [0,0,0,0, 0]
teams = [0, 0, 0, 0, 0]
papers = [0,0,0,0, 0]
counts = 0
def getStats(venueDir):
    global counts, dataPos, dataPosNorm, dataParts
    for dirFile in os.listdir(venueDir):
        if not os.path.isdir(venueDir + '/' + dirFile):
            continue
        for dataFile in os.listdir(venueDir + '/' + dirFile):
            lowestPos = 0
            reader = csv.reader(open(venueDir + '/' + dirFile + '/' + dataFile))
            next(reader)
            numParts = 0
            for row in reader:
                numParts += 1
                if int(row[7]) > lowestPos:
                    lowestPos = int(row[7])
            if lowestPos < 2:
                continue 
            foundAteam = False
            taskPos = [0,0,0,0, 0]
            taskPosNorm = [0,0,0,0, 0]
            taskParts = [0,0,0,0, 0]
            taskCounts = [0,0,0,0, 0]
            reader = csv.reader(open(venueDir + '/' + dirFile + '/' + dataFile))
            next(reader)
            for row in reader:
                if row[7] == '0':
                    continue
                for i in range(0,4):
                    if row[i+1] == '1':
                        foundAteam = True
                        taskPos[i] += int(row[7])
                        taskPosNorm[i] += (int(row[7]) -1) / (lowestPos - 1)
                        taskParts[i] += numParts
                        taskCounts[i] += 1
                        teams[i] += 1
                        if row[5] == '1':
                            papers[i] += 1
                taskPos[4] += int(row[7])
                taskPosNorm[4] += (int(row[7]) -1) / (lowestPos - 1)
                taskParts[4] += numParts
                taskCounts[4] += 1
                teams[4] += 1
                if row[5] == '1':
                    papers[4] += 1
            if foundAteam:
                for i in range(0,5):
                    if taskCounts[i] != 0:
                        dataPos[i] += taskPos[i] / taskCounts[i]
                        dataPosNorm[i] += taskPosNorm[i] / taskCounts[i] 
                        dataParts[i] += taskParts[i] / taskCounts[i]
                counts += 1
             
def printStats():
    names = ['no company', 'company', 'some company', 'organiser', 'all']
    print(counts, "tasks")

    print("note that the number of papers is not fair yet, SemEval annotation is not always correct here! (teams with no paper are to often annotated as no company)\n")
    print('category\t #teams  avg pos  normed  avg parts  papers')
    for i in range(5):
        dataPos[i] = dataPos[i] / counts
        dataPosNorm[i] = dataPosNorm[i] / counts
        dataParts[i] = dataParts[i] / counts
        ratio = 0.0
        if teams[i] > 0:
            ratio = papers[i]/teams[i]
        print(" {0}\t {1:.2f}\t{2:.2f} \t{3:.2f} \t{4:.2f} \t\t{5:.2f}".format(names[i], teams[i], dataPos[i], dataPosNorm[i], dataParts[i], ratio))

for i in range(1, len(sys.argv)):
    getStats(sys.argv[i])

printStats()

