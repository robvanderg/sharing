import csv
import sys
import os

def values(csvRow):
    for i in range(1, len(csvRow)):
        if csvRow[i] == '':
            csvRow[i] = '0'
        try:
            csvRow[i] = int(csvRow[i])
        except:
            return False
    return True

    
def getTasks(csvPath, tgtDir):
    if not os.path.isdir(tgtDir):
        os.mkdir(tgtDir) 
    reader = csv.reader(open(csvPath))
    data = {}
    writeFile = open('tmp', 'w')
    writer = csv.writer(writeFile)
    for row in reader:
        if not values(row):
            writeFile.close()
            year = row[0][0:4]
            if not os.path.isdir(tgtDir + '/' + year):
                os.mkdir(tgtDir + '/' + year)
            writeFile = open(tgtDir + '/' + year + '/' + row[0][5:] + '.csv'.strip(), 'w')
            writer = csv.writer(writeFile)
        writer.writerow(row)
           
if len(sys.argv) < 2:
    print("please give src csv and dst folder")
 
getTasks(sys.argv[1], sys.argv[2])
if os.path.isfile('tmp'):
    os.remove('tmp')

