"""
This script assumes results from scripts/generalStats are in directory results. 
Run this first:
python3 scripts generalStats.py wmt > results/wmt
python3 scripts generalStats.py semeval > results/semeval 
...
"""
import matplotlib.pyplot as plt

allData = {}
def getScores (resultPath):
    global allData
    name = resultPath[resultPath.rfind('/') + 1:]
    data = []
    for line in open(resultPath):
        splitted = line.split()
        if len(splitted) < 1:
            continue
        if splitted[0] == 'no' or splitted[0] == 'some':
            data.append(float(splitted[3]))
        elif splitted[0] == 'company' or splitted[0] == 'organiser':
            data.append(float(splitted[2]))
    allData[name] = data

getScores('results/semeval')
getScores('results/clef')
getScores('results/conll')
getScores('results/wmt')
getScores('results/evalita')

bar_width = 0.15

colors = ['blue', 'red', 'green', 'yellow', 'purple']
counter = 0
allDataOrdered = ['evalita', 'clef', 'semeval', 'conll', 'wmt']
for venue in allDataOrdered:
    index = []
    for i in range(len(allData[venue])):
        index.append(i + (bar_width * counter))
    print(index)
    print(allData[venue])
    print()
    plt.bar(index, allData[venue], bar_width, label=venue, color=colors[counter])
    plt.plot([])
    counter += 1
plt.xticks([.35,1.35,2.35,3.35], ['no company', 'company', 'some company', 'organiser'])
plt.legend(loc = 'upper center')
plt.ylabel('Average normalized ranking')
plt.savefig('results.pdf')
plt.show()
