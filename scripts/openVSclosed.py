import csv
import sys
import os

data = {}

def getStats(venueDir):
   for dirFile in os.listdir(venueDir):
        if not os.path.isdir(venueDir + '/' + dirFile):
            continue
        if dirFile not in data:
            data[dirFile] = [0, 0]
        for dataFile in os.listdir(venueDir + '/' + dirFile):
            reader = csv.reader(open(venueDir + '/' + dirFile + '/' + dataFile))
            first = True
            for row in reader:
                if first:
                    first = False
                    continue
                if row[6] == '1':
                    data[dirFile][0] += 1
                else:
                    data[dirFile][1] += 1
    
def printStats():
    print("raw counts:")
    for dataThing in sorted(data):
        print (dataThing, '\t', data[dataThing][1], '\t',  data[dataThing][0])
    print()

    print("percentage open:")
    for dataThing in sorted(data):
        print (dataThing, '\t', data[dataThing][1] / (data[dataThing][1] + data[dataThing][0]))
    print()

    print('total')
    total = [0,0]
    for dataThing in sorted(data):
        total[0] += data[dataThing][0]
        total[1] += data[dataThing][1]
    print(total[0], total[1])
    print(total[1] / (total[0] + total[1]))
getStats('clef')
getStats('conll')
getStats('evalita')
getStats('semeval')
getStats('wmt')

printStats()

